import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * An Application of the WordCounter
 * @author Arut Thanomwatana
 *
 */
public class WordCount 
{
	/**
	 * Test the WordCounter with Alice in the wonderland.
	 * @param args is not used
	 * @throws IOException if URL is null.
	 */
	public static void main(String [] args)throws IOException
	{
		final String DELIMS = "[\\s,.\\?!\"():;]+"; 
		String FILE_URL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
		URL url = new URL(FILE_URL);
		InputStream input;
		input = url.openStream();
		Scanner scanner = new Scanner(input);
		scanner.useDelimiter(DELIMS);
		String result= "";
		while(scanner.hasNext()){
			result+=scanner.next()+" ";

		}
		String [] split = result.split(" ");
		WordCounter check = new WordCounter();
		for(int i =0;i<split.length;i++)
			check.addWord(split[i]);
		
		for(int i =0;i<20;i++)
			System.out.println(check.getSortedWords()[i]+" "+check.getCount(check.getSortedWords()[i]));

	
		
	}


}
