import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A WordCounter that count frequency of a word.
 * 
 * @author Arut Thanomwatana
 *
 */
public class WordCounter 
{
	/** occurence is a map used to keep the word. */
	private Map<String,Integer> occurence;

	/**
	 * Initialize the WordCounter.
	 */
	public WordCounter()
	{
		occurence = new HashMap<String,Integer>();
	}

	/**
	 * Add a Word into word counter.
	 * @param word is a word that will be add into a word counter
	 */
	public void addWord(String word){
		if(occurence.containsKey(word.toLowerCase())){
			
			occurence.put(word.toLowerCase(),occurence.get(word.toLowerCase())+1);
		}
		else
			occurence.put(word.toLowerCase(),1);
	}
	/**
	 * Get the set of word from map.
	 * @return Set of word.
	 */
	public Set<String> getWords(){
		return occurence.keySet();

	}
	/**
	 * Get the frequency of 1 word.
	 * @param word is a word that will get a frequency
	 * @return frequency of the word
	 */
	public int getCount(String word){
		return occurence.get(word);

	}
	/**
	 * Get the set of word that already sort.
	 * @return Array of sort word
	 */
	public String[] getSortedWords()
	{
		List<String> tempList = new <String> ArrayList(this.getWords());
		Collections.sort(tempList);
		String [] returnList = new String[tempList.size()];
		for(int i =0;i<returnList.length;i++)
			returnList[i]=tempList.get(i);
		return returnList;
		
	}

}
